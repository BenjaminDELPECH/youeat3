import {useHistory} from "react-router";


function MealItem({ item }) {
    console.log(item);
    const history = useHistory();
    const onClickItem=()=>{
        const url = "/meal/"+item.id
        history.push(url)
    }
    if (item) {
        return (
            <div className="clickable paddingM" onClick={onClickItem}>
                {item.name}
            </div>
        )
    }
    return null;
}

export default MealItem;