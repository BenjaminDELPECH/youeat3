import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";
import useSWR from "swr";
import {RootState} from "../store";
import authSlice from "../store/slices/auth";
import {UserResponse} from "../types";
import {fetcher} from "../utils/axios";

const Profile = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const account = useSelector((state: RootState) => state.auth.account);

  const userId = account?.id;

  const user = useSWR<UserResponse>(`/users/${userId}/`, fetcher)

  const handleLogout = () => {
    dispatch(authSlice.actions.logout());
    localStorage.removeItem("refreshToken");
    history.push("/login");
  };
  return (
    <div >
      <div >
        <button
          onClick={handleLogout}

        >
          Deconnexion
        </button>
      </div>
      <div>
        <p >Welcome {user.data ?
          <div>{user.data.username}</div> :
          <div>Loading...</div>
        }
        </p>

      </div>
    </div>
  );
};

export default Profile;