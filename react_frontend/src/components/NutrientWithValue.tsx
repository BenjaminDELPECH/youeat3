import {NutrientFood, NutrientWithValue} from "../types";

export default function NutrientWithValueComponent(props: NutrientWithValue) {
    const { nutrient, nutrientFoods } = props;
    let { nameFr, besoin, unit } = nutrient;
    unit = " " + unit;
    let sumValue: number = 0;
    for (let i = 0; i < nutrientFoods.length; i++) {
        const nutrientFood: NutrientFood = nutrientFoods[i];
        const { value } = nutrientFood;
        sumValue += value;
    }
    let barColorClass = "";
    let percentage;
    sumValue = Math.round(sumValue * 10) / 10;
    if (besoin > 0) {
        percentage = (sumValue / besoin) * 100;
        percentage = Math.round(sumValue * 10) / 10;
        percentage = percentage > 100 ? 100 : percentage;

        if (percentage < 33) {
            barColorClass = "redTheme";
        } else if (percentage < 66) {
            barColorClass = "orangeTheme";
        } else if (percentage < 90) {
            barColorClass = "greenOrangeTheme";
        } else {
            barColorClass = "greenTheme";
        }
    }
    const stylePercentage = {
        width: percentage + "%",
    };
 

    const barClasses = "barPercentage " + barColorClass;


    return (
        <div className="container-row">
            <div className="row">{nameFr}</div>
            <div className="row barPercentageContainer">
                <div className={barClasses} style={stylePercentage}>
                    <div className="valueUnit row">
                        {sumValue}
                        {besoin > 0 ? "/" + besoin : null}
                        {unit}

                    </div>
                </div>
            </div>
        </div>
    )
}