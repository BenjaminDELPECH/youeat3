import React, {useState} from 'react';
import * as Yup from "yup";
import {useFormik} from "formik";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";
import authSlice from '../store/slices/auth';
import {getApiUrl} from '../utils/utils';
import axiosService from '../utils/axios';

const SIGNIN_MODE = "in";
const SIGNUP_MODE = "up";
function Login() {

    const [message, setMessage] = useState("");
    const [signMode, setSignMode] = useState(SIGNIN_MODE);
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();

    const handleLogin = (email: string, password: string) => {
        let url = getApiUrl() + "/auth/";
        if(signMode===SIGNUP_MODE){
            url += "register/";
        }else if(signMode===SIGNIN_MODE){
            url += "login/";
        }
    
        

        const body = { username: email, email, password };

        axiosService.post(url, body)
            .then((response) => {
                const { access, refresh, user } = response.data;
                localStorage.setItem("refreshToken", refresh);
                dispatch(
                    authSlice.actions.setAuthTokens({
                        token: access,
                        refreshToken: refresh
                    })
                );
                dispatch(authSlice.actions.setAccount(user));
                setLoading(false);
                history.push("/");
            })
            .catch(error => {
                setMessage(error.response.data.detail.toString())
            })
            .finally(() => setLoading(false));
    };

    const formik = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        onSubmit: (values) => {
            setLoading(true);
            handleLogin(values.email, values.password);
        },
        validationSchema: Yup.object({
            email: Yup.string().trim().required("Le nom d'utilisateur est requis"),
            password: Yup.string().trim().required("Le mot de passe est requis")
        })
    });

    let pageTitle;
    if (signMode === SIGNIN_MODE) {
        pageTitle = "Log in to your account";
    } else if (signMode === SIGNUP_MODE) {
        pageTitle = "Create a new account";
    }

    return (
        <div className="container">
            <div><strong>{pageTitle}</strong></div>
            <form onSubmit={formik.handleSubmit}>
                {/* email */}
                <input
                    id="email"
                    type="email"
                    placeholder="Email"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />

                {formik.errors.email ? <div>{formik.errors.email} </div> : null}

                {/* password */}
                <input
                    id="password"
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                /><div>
                    {formik.errors.password ? (
                        <div>{formik.errors.password} </div>
                    ) : null}
                </div>

                {/* message */}
                <div className="text-danger text-center my-2" hidden={false}>
                    {message}
                </div>

                {/* submit */}
                <button type="submit"
                    disabled={loading}>
                    {signMode === SIGNIN_MODE ? "Login" : "Register"}
                </button>

            </form>
            <div className="margin-M-top">
                {signMode === SIGNUP_MODE ? <div>Already an account ? <button onClick={() => setSignMode(SIGNIN_MODE)} disabled={loading}>Login</button></div> : null}
                {signMode === SIGNIN_MODE ? <div>New User ? <button onClick={() => setSignMode(SIGNUP_MODE)} disabled={loading}>Register</button></div> : null}
            </div>
        </div>
    )

}

export default Login;


