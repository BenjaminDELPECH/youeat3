import {useSelector} from "react-redux";
import {RootState} from "../store";

export default function Header() {
    const links = [
        { "label": "Meal Creator", "url": `/` },
        { "label": "My Meals", "url": "/myMeals" },
        { "label": "Profile", "url": "/profile" }
    ];
    const linkOnClick = (url: string) => {
        window.location.href = url;
    };
    const account = useSelector((state: RootState) => state.auth.account);

    const username = account?.username;
  
    return (<div className="header row">
        <div className="links row">
            {links.map(item => {
                return (
                    <div key={item.label} className="link" onClick={(() => { linkOnClick(item.url) })}>
                        {item.label}
                    </div>
                )
            })}
         
        </div>
        <div className="">{username}</div>
    </div>
    )
}