import useSWR from "swr"
import {NutrientFood, NutrientResponse, NutrientStatsProps, NutrientWithValue} from "../types"
import {fetcher} from "../utils/axios"
import NutrientWithValueComponent from "./NutrientWithValue";

export default function NutrientStats(props: NutrientStatsProps) {
    const { meal_id } = props;


    //request nutrients
    const nutrients = useSWR<NutrientResponse[]>('/nutrients/', fetcher);

    //request nutrientFoods
    const nutrientFoods = useSWR<NutrientFood[]>('nutrientFoods?meal_id=' + meal_id, fetcher);

    const nutrientWithValues: NutrientWithValue[] = [];
    if(nutrients.data){
        for (let i = 0; i < nutrients.data.length; i++) {
            const nutrient = nutrients.data[i];
            const nutrientWithValue:NutrientWithValue = {
                nutrient: nutrient,
                nutrientFoods: []
            }
            nutrientWithValues.push(nutrientWithValue);
        }
    }
    if (nutrientFoods.data && nutrients.data) {
        console.log(nutrientFoods.data);

        for (let i = 0; i < nutrientFoods.data.length; i++) {
            const nutrientFood = nutrientFoods.data[i];
            const { nutrient: nutrientId } = nutrientFood;
            let nutrientWithValue: NutrientWithValue | undefined = nutrientWithValues.find(e => e.nutrient.id === nutrientId);
            if (nutrientWithValue) {
                nutrientWithValue.nutrientFoods.push(nutrientFood);

            }
        }
    }

    return (
       
            <div>
                <div><strong>Nutrient Stats</strong></div>
                <div>{nutrientWithValues ?
                    nutrientWithValues.map((nutrientWithValue: NutrientWithValue) => {
                        return (<NutrientWithValueComponent
                            nutrient={nutrientWithValue.nutrient}
                            nutrientFoods={nutrientWithValue.nutrientFoods}
                        />)
                    })
                    : <div>loading</div>}</div>

            </div>
     
    )
};