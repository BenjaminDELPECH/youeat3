import SimpleList from './SimpleList'
import MealItem from './MealItem'

function Meals(){
    return (
        <SimpleList 
        urlToFetch="/meals"
        ItemRenderer={MealItem}
        />
    )

}

export default Meals;