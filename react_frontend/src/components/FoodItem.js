import {useState} from "react";
import {useHistory} from "react-router";
import axiosService from "../utils/axios";
import {getApiUrl} from "../utils/utils";


function FoodItem({ item, handleAdd, handleDelete }) {
    const [addLoading, setAddLoading] = useState(false)
    const history = useHistory();
    const url = window.location.href.split("/")
    const mealId = url[url.length-1]
    const onClickItem=()=>{
        const url2 = "/food/"+item.id
        history.push(url2)
    }
    const addFoodToMeal=(event)=>{
        event.stopPropagation()
        setAddLoading(true);
        const url2 =  getApiUrl()+"/linkMealFoods/";
        const postData = {
            "meal": parseInt(mealId),
            "food": item.id
        };

        axiosService.post(url2,postData)
        .then((resp)=>handleAdd())
        .catch(err=>console.error(err))
        .finally(()=>{setAddLoading(false)})
    }
    let addTemplate;
    if(addLoading){
        addTemplate = (<div>...</div>)
    }else if(handleAdd){
        addTemplate = (<div className=" clickable" onClick={addFoodToMeal}>Add</div>)
    }

    let deleteTemplate;
    if(handleDelete){
        deleteTemplate = (<div className=" clickable" onClick={handleDelete}>Delete</div>)
    }
    if (item) {
        return (
            <div className="row" >
                <div className="row" onClick={onClickItem}>{item.name}</div>
                {addTemplate}
                {deleteTemplate}
            </div>
        )
    }
    return null;
}

export default FoodItem;