import React from "react";
import {Col, Progress, Row, Table} from "antd";


export function  Meal2() {
    const columns = [
        {
            title: 'Aliment',
            dataIndex: 'food',
            key: 'food',
            render: (text: string) => <span>{text}</span>

        },
        {
            title: 'quantité',
            dataIndex: 'quantity',
            key: 'quantity'
        },
        {
            title: 'portion',
            dataIndex: 'portion',
            key: 'portion',
            render: (text: string) => <span>{text}</span>
        }
    ];

    interface FoodData {
        key: number,
        food: string,
        portion: string,
        quantity: string
    }

    const data: FoodData[] = [
        {
            key: 0,
            food: 'Sarrasin',
            portion: '100g',
            quantity: "1x"

        },
        {
            key: 0,
            food: 'Sarrasin',
            portion: '100g',
            quantity: "1x"

        },
        {
            key: 0,
            food: 'Sarrasin',
            portion: '100g',
            quantity: "1x"

        },
        {
            key: 0,
            food: 'Sarrasin',
            portion: '100g',
            quantity: "1x"

        },
        {
            key: 0,
            food: 'Sarrasin',
            portion: '100g',
            quantity: "1x"

        },
    ]
    const title = ()=><Row>
        <Col span={16}><strong>Meal 1</strong></Col>
        <Col span={8}>
            <Row>
                <Col flex="none">Calories : &nbsp;</Col>
                <Col flex="auto"><Progress percent={56}  /></Col>
            </Row>

        </Col>
    </Row>
    return (
            <Table
                showHeader={false}
                title={title}
                columns={columns}
                dataSource={data}
                pagination={false}
                bordered={true}
                size={"small"}
                />
    )
}