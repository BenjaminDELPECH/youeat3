import React, {useState} from "react";
import {Col, Form, InputNumber, Radio, Row, Slider} from "antd";
import {Meal2} from "./Meal2";
import {NutrientData} from "../models/NutrientData";
import {NutrientListStat} from "./NutrientListStat";


function SliderComponent(props: { min: number, max: number, label: string, onChange: any, value: number }) {
    const {min, max, label, onChange, value} = props;

    return (
        <Form.Item><Row>
            <Col span={24}>{label}</Col>
            <Col span={24}>
                <Row>
                    <Col span={18}>
                        <Slider
                            min={min}
                            max={max}
                            value={value}
                            onChange={onChange}
                        />
                    </Col>
                    <Col span={6}>
                        <InputNumber
                            min={min}
                            max={max}
                            value={value}
                            style={{width: "50px"}}
                            onChange={onChange}
                        />
                    </Col>
                </Row>
            </Col>
        </Row>
        </Form.Item>);
}


/*<div>
                    <div>Few parameters...</div>
                    <div>Age :</div>
                    <div>Height :</div>
                    <div>Weight :</div>
                    <div>Sex :</div>
                    <div>Step per day</div>
                    <div>Loved foods</div>
                    <div>Liked foods</div>
                    <div>Hated foods</div>
                    <div>Diet style :(paléo, keto,...)</div>
                    <div>Goal weight : -2/2kg / month</div>
                    <div>Work times: [create new interval] </div>
                    <div>Work out times : [create new interval] [intensity]</div>
                    <div>Glycemic loa</div>
                </div>*/


export function MealCreator() {
    const [height, setHeight] = useState(170);
    const [age, setAge] = useState(50);
    const [weight, setWeight] = useState(50);
    const [gender, setGender] = useState(1);

    const [carbohydrateProportion, setCarbohydrateProportion] = useState(33);
    const [lipidsProportion, setLipidProportion] = useState(33);
    const [proteinProportion, setProteinProportion] = useState(33);

    const nutrientDataList: NutrientData[] = [
        {
            "id": 1,
            "name": "Carbohydrate",
            "value": 40,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 2,
            "name": "Lipids",
            "value": 10,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Proteins",
            "value": 100,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Fibers",
            "value": 78,
            "need": 100,
            "unity": "g"
        }
    ];
     const nutrientDataList2: NutrientData[] = [
        {
            "id": 1,
            "name": "Carbohydrate",
            "value": 40,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 2,
            "name": "Lipids",
            "value": 10,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Proteins",
            "value": 100,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Fibers",
            "value": 78,
            "need": 100,
            "unity": "g"
        },
         {
            "id": 1,
            "name": "Carbohydrate",
            "value": 40,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 2,
            "name": "Lipids",
            "value": 10,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Proteins",
            "value": 100,
            "need": 100,
            "unity": "g"
        },
        {
            "id": 3,
            "name": "Fibers",
            "value": 78,
            "need": 100,
            "unity": "g"
        }
    ]
    return (
        <Row>
            <Col span={5} className="gutter-row left-filters" style={{minWidth: "200px"}}>
                <Row justify={"center"}>
                    <Col span={24} className={"p-m"}>
                        <div>
                            <Form.Item>
                                <Radio.Group onChange={(e) => setGender(e.target.value)} value={gender}>
                                    <Radio value={1}>Male</Radio>
                                    <Radio value={0}>Female</Radio>
                                </Radio.Group>
                            </Form.Item>

                            <SliderComponent min={0} max={220} label={"Height"} onChange={setHeight} value={height}/>
                            <SliderComponent min={0} max={120} label={"Age"} onChange={setAge} value={age}/>
                            <SliderComponent min={0} max={150} label={"Weight"} onChange={setWeight} value={weight}/>
                        </div>


                        <SliderComponent min={0} max={220} label={"Carbohydrates"}
                                         onChange={setCarbohydrateProportion}
                                         value={carbohydrateProportion}/>
                        <SliderComponent min={0} max={120} label={"Lipids"} onChange={setLipidProportion}
                                         value={lipidsProportion}/>
                        <SliderComponent min={0} max={150} label={"Proteins"} onChange={setProteinProportion}
                                         value={proteinProportion}/>
                    </Col>
                </Row>
            </Col>
            <Col span={19} className="gutter-row" style={{overflow: "auto"}}>
                <Row>
                    <Col span={18}>
                        <Row justify={"center"}>
                            <Col span={22}>
                                <Row gutter={[16, 16]}>
                                    <Col span={24} className="gutter-row">
                                        <Meal2/>
                                    </Col>
                                    <Col span={24} className="gutter-row">
                                        <Meal2/>
                                    </Col>
                                    <Col span={24} className="gutter-row">
                                        <Meal2/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={6} className="gutter-row ">
                        <Row justify={"center"}>
                            <Col span={22}>
                                <Row gutter={[16, 16]}>
                                    <Col span={24} className="gutter-row">
                                        <NutrientListStat nutrientDataList={nutrientDataList}/>
                                    </Col>
                                    <Col span={24} className="gutter-row">
                                        <NutrientListStat nutrientDataList={nutrientDataList2}/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
};

