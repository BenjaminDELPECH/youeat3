import axios from "axios";
import {useState} from "react";
import {getApiUrl} from "../utils/utils";

export default function Search({ searchUrl, ItemRenderer, handleListUpdated }) {
    const [search, setsearch] = useState("");
    const [searchResult, setsSearchResult] = useState([]);
    const [loading, setLoading] = useState(false);
    const [hasSearched, setHasSearched] = useState(false);
    const searchChange = () => {
        setLoading(true);
        setHasSearched(false);
        const url = getApiUrl() + searchUrl + search;

        axios.get(url)
            .then((response => {
                setsSearchResult(response.data)
            }))
            .catch(err => console.error(err))
            .finally(() => { setLoading(false); setHasSearched(true); });
    }

    let searchResultTemplate;
    if (searchResult.length) {
        searchResultTemplate = (
            <div class="">
                <div className="container">
                    <div className="row">
                        <div className="row">{searchResult.length} Resultat(s)</div>
                        <div className="clickable close" onClick={()=>setsSearchResult([])}>X</div>
                    </div>
                    <div>---------------------------</div>
                    {searchResult.map(item => {
                        return <ItemRenderer key={item.id} item={item} handleAdd={handleListUpdated} />
                    })}
                </div>
            </div>
        )
    } else if (hasSearched) {
        searchResultTemplate = (<div>no results</div>);
    }


    if (loading) {
        return (
            <div>loading...</div>
        )
    } else {
        return (
            <div>
                <input
                    type="text"
                    placeholder="search foods.."
                    value={search}
                    onChange={(e) => setsearch(e.target.value)}
                />
                <button name="test" onClick={searchChange}>Search</button>
                {searchResultTemplate}
            </div>
        )
    }

}