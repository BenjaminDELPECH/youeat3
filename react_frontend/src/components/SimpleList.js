import React from "react";
import useSWR from "swr";
import {fetcher} from "../utils/axios";

function SimpleList({urlToFetch, ItemRenderer}){
    const list = useSWR(urlToFetch, fetcher);
    if(list.data){
        return (
            <div>
                {list.data.map(item=>{
                    return  <ItemRenderer key={item.id} item={item} />
                })}
            </div>
        )
    }else{
        return (
            <div>empty list...</div>
        )
    }
}

export default SimpleList;