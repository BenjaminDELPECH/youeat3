import {useState} from "react";
import {useSelector} from "react-redux";
import {useSWRConfig} from "swr";
import {RootState} from "../store";
import axiosService from "../utils/axios";
import MealItem from "./MealItem";
import SimpleList from "./SimpleList";

export default function MyMeals() {
    const user = useSelector((state: RootState) => state.auth.account)
    const userId = user?.id
    const urlToFetch = "/meals?user_id=" + userId
    const { mutate } = useSWRConfig()

    const [mealName, setmealName] = useState("")
    const handleAddMeal = () => {
        const postJson = {
            "name": mealName,
            "user": userId
        }
        axiosService.post('/meals/', postJson)
            .then(() => mutate(urlToFetch))
            .catch((err) => console.error(err))
            .finally(() => setmealName(""))
    }
    return (
        <div>
            <input
                type="text"
                value={mealName}
                onChange={(e) => setmealName(e.target.value)}

            />
            <input type='submit' onClick={handleAddMeal} value="Ajouter repas" ></input>
            <SimpleList
                urlToFetch={urlToFetch}
                ItemRenderer={MealItem}
            />
        </div>
    )

}