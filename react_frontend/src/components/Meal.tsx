import useSWR, {useSWRConfig} from "swr";
import {MealResponse} from "../types";
import axiosService, {fetcher} from "../utils/axios";
import FoodItem from "./FoodItem";
import NutrientStats from "./NutrientStats";
import Search from "./Search";

const Meal = () => {
    const foodSearchUrl = "/foods?search=";
    const url = window.location.href.split("/")
    const mealId = url[url.length - 1];
    const mealidInt = parseInt(mealId);
    const mealUrl = ('/meals/' + mealId);
    const mealStatsUrl = "nutrientFoods?meal_id=" + mealidInt;
    const meal = useSWR<MealResponse>(mealUrl, fetcher);
    const { mutate } = useSWRConfig()
    const handleMealUpdated = () => {
        mutate(mealUrl)
        mutate(mealStatsUrl)
    }
    const handleDelete = (linkMealFoodId: string) => {
        const deleteLinkMealFoodUrl = "linkMealFoods/" + parseInt(linkMealFoodId)
        axiosService.delete(deleteLinkMealFoodUrl)
            .then(resp => handleMealUpdated())
    }
    let mealTemplate;
    console.log(meal.data)
    if (meal && meal.data && meal.data.meal_to_foods.length) {
        const items = meal.data.meal_to_foods.map(e => {
            return { "food": e.food, "linkMealFoodId": e.id }
        })
        mealTemplate = (<div>
            {items.map(item => {
                return <FoodItem
                    key={item.linkMealFoodId}
                    item={item.food}
                    handleAdd={null}
                    handleDelete={() => handleDelete(item.linkMealFoodId)}
                />
            })}
        </div>)
    }

    return (
        <div className="row">
            <div className="container col-6">
                <Search searchUrl={foodSearchUrl} handleListUpdated={handleMealUpdated} ItemRenderer={FoodItem} />
                <div className="">
                    <div className="row"><strong>Aliments</strong></div>
                    {mealTemplate}

                </div>
            </div>
            <div className="container col-6">
                <NutrientStats meal_id={mealidInt} />
            </div>
        </div>
    );
}

export default Meal;