import './App.css';

import Login from './components/Login';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {PersistGate} from 'redux-persist/integration/react';
import ProtectedRoute from './routes/ProtectedRoute';
import Profile from './components/Profil';
import MyMeals from './components/MyMeals';
import Meal from './components/Meal';
import store, {persistor} from './store';
import {MealCreator} from "./components/MealCreator";
import 'antd/dist/antd.css';

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        {/*<Header />*/}
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/" component={MealCreator} />
            <Route path="/meal/" component={Meal} />
            <ProtectedRoute exact path="/myMeals" component={MyMeals} />
            <ProtectedRoute exact path="/profile" component={Profile} />
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
}

