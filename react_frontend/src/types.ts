export interface AccountResponse {
    user:UserResponse,
    access: string;
    refresh: string;
  }
  
export interface UserResponse {
    id: string;
    email: string;
    username: string;
    is_active: boolean;
    date_joined: Date;
    last_login: Date;
    meal_listing:string;
}

export interface FoodResponse{
  id:string;
  name:string;
}

export interface LinkMealFoodResponse{
  id:string;
  food: FoodResponse;
  meal:string;
}

export interface MealResponse{
  id:string;
  name:string;
  meal_to_foods:[LinkMealFoodResponse]
}

export interface NutrientResponse{
  id:number;
  nameFr:string;
  unit:string;
  besoin:number;
}

export interface NutrientFood{
  id:number;
  nutrient:number;
  food:number;
  value:number;
}

export interface NutrientWithValue{
  nutrient:NutrientResponse;
  nutrientFoods:NutrientFood[];
}

export interface NutrientStatsProps{
  meal_id:number;
}