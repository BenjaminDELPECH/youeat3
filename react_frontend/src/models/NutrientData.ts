export interface NutrientData {
    id: number,
    name: string,
    value: number,
    need: number,
    unity: string
}
