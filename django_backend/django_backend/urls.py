"""django_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from core.views import GroupViewSet, UserViewSet
from core.views import LoginViewSet, RefreshViewSet, RegistrationViewSet
from nutrients.views import NutrientFoodsViewSet
from meal.views import MealViewSet, FoodViewSet, LinkMealFoodViewSet
from nutrients.views import NutrientViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'meals', MealViewSet)
router.register(r'foods', FoodViewSet)
router.register(r'linkMealFoods', LinkMealFoodViewSet)
router.register(r'nutrients', NutrientViewSet)
router.register(r'nutrientFoods', NutrientFoodsViewSet)

router.register(r'auth/login', LoginViewSet, basename='auth-login')
router.register(r'auth/register', RegistrationViewSet, basename='auth-register')
router.register(r'auth/refresh', RefreshViewSet, basename='auth-refresh')

urlpatterns = [
    path('django/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
