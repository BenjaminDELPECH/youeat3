from django.db import models

from meal.models import Food



# Create your models here.
class NutrientGroup(models.Model):
    nameFr = models.CharField(max_length=255, default="")
    nameEn = models.CharField(max_length=255, default="")
    deployed_default = models.BooleanField(default=False)
    position = models.IntegerField(default=0)


class Nutrient(models.Model):
    nameFr = models.CharField(max_length=255, default="nutrient_name_fr_default")
    familiarNameFr = models.CharField(max_length=255, default="")
    nameEn = models.CharField(max_length=255)
    unit = models.SlugField(null=True)
    besoin = models.CharField(default=0, max_length=255)
    besoinMax = models.CharField(default=0, max_length=255)
    menNeed = models.DecimalField(max_digits=10, decimal_places=5, null=True)
    womenNeed = models.DecimalField(max_digits=10, decimal_places=5, null=True)
    max = models.DecimalField(max_digits=10, decimal_places=5, null=True)
    sportivAdditionnalNeed = models.IntegerField(default=0, null=True)
    code = models.CharField(max_length=255, null=True)
    nutrientGroup = models.ForeignKey(NutrientGroup, on_delete=models.SET_DEFAULT, default=None, null=True)
    nutrientGroupTitle = models.BooleanField(default=False)
    needPerKg = models.BooleanField(default=False, null=True)


    def __str__(self):
        label = self.nameFr
        if len(self.familiarNameFr) > 0:
            label = self.familiarNameFr
        return '%s' % (label)


class NutrientNeeds(models.Model):
    nutrient = models.ForeignKey(Nutrient, on_delete=models.CASCADE)
    sex = models.IntegerField(default=-1)
    need  = models.FloatField(default=0.0)
    code = models.CharField(default = " ", max_length=255, null = True)



class NutrientCategory(models.Model):
    icon_name = models.CharField(max_length=255)
    category_name_fr = models.CharField(max_length=255)
    category_name_en = models.CharField(max_length=255)
    activated = models.BooleanField(default=True)

class NutrientFood(models.Model):
    nutrient = models.ForeignKey(Nutrient, related_name='nutrient_to_foods', on_delete=models.CASCADE)
    food = models.ForeignKey(Food, related_name='food_to_nutrients', on_delete=models.CASCADE)
    value = models.FloatField()

    class Meta:
        indexes = [
            models.Index(fields=['food_id']),
            models.Index(fields=['nutrient_id']),
        ]
    