from django.shortcuts import render
from rest_framework import serializers, viewsets
from rest_framework.decorators import permission_classes
from nutrients.serializers import NutrientFoodSerializer
from nutrients.models import NutrientFood
from meal.serializers import MealSerializer

from nutrients.models import Nutrient
from nutrients.serializers import NutrientSerializer

# Create your views here.
class NutrientViewSet(viewsets.ModelViewSet):
    queryset = Nutrient.objects.all()
    serializer_class = NutrientSerializer
    permission_classes = []

    
    
class NutrientFoodsViewSet(viewsets.ModelViewSet):
    queryset = NutrientFood.objects.all()
    serializer_class = NutrientFoodSerializer
    permission_classes = []

    def get_queryset(self):
        meal_id = self.request.query_params.get('meal_id')
        if meal_id is not None:
            self.serializer_class = NutrientFoodSerializer
            sql_request_str = "SELECT * FROM nutrients_nutrientfood where food_id in (Select food_id FROM meal_linkmealfood WHERE meal_id = {0})".format(meal_id)
            result =  NutrientFood.objects.raw(sql_request_str)
            return result