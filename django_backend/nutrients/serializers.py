from rest_framework import serializers

from nutrients.models import Nutrient, NutrientFood

class NutrientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nutrient
        fields = ['id', 'nameFr', 'unit', 'besoin']

class NutrientFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = NutrientFood
        fields = ['id', 'nutrient', 'food', 'value']

