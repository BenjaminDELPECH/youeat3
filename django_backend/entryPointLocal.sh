python ./manage.py migrate -v 3 && gunicorn django_backend.wsgi:application --bind 0.0.0.0:$DJANGO_PORT  --timeout=120 --workers=3 --log-level=debug
