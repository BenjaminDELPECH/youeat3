from django.contrib.auth import models
from rest_framework import serializers
from meal.models import Meal, Food, LinkMealFood
from rest_framework.validators import UniqueTogetherValidator

class FoodSearializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ['id', 'name']

class LinkMealFoodDetailSerializer(serializers.ModelSerializer):
    food = FoodSearializer()
    class Meta:
         model = LinkMealFood
         fields = ['id', 'meal', 'food']

class LinkMealFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = LinkMealFood
        fields = ['meal', 'food']
        validators = [
            UniqueTogetherValidator(
                queryset=LinkMealFood.objects.all(),
                fields = ['food', 'meal']
            )
        ]

class MealDetailSerializer(serializers.ModelSerializer):
    meal_to_foods = LinkMealFoodDetailSerializer(many=True)

    class Meta:
        model = Meal
        fields = ['id', 'name', 'user', 'meal_to_foods']

class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = ['id', 'name', 'user']