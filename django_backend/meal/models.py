from django.db import models
from core.models import ModelTracked
from django.contrib.auth.models import User
import json

# Create your models here.
class Meal(ModelTracked):
    user = models.ForeignKey(User, related_name='meals',on_delete=models.CASCADE)
    name = models.TextField(max_length=255)

    def __str__(self) -> str:
        data = {}
        data['id'] = self.id
        data['name'] = self.name
        return str(data)

class Food(models.Model):
    name = models.TextField(max_length=255)
    last_updated = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

class LinkMealFood(models.Model):
    meal = models.ForeignKey(Meal, related_name='meal_to_foods',on_delete=models.CASCADE)
    food = models.ForeignKey(Food, related_name='food_to_meals',on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
