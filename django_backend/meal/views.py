
from django.db.models import query
from rest_framework.decorators import permission_classes
from rest_framework.mixins import CreateModelMixin
from meal.serializers import MealDetailSerializer
from nutrients.serializers import NutrientFoodSerializer
from nutrients.models import NutrientFood
from meal.serializers import LinkMealFoodDetailSerializer
from meal.models import Meal, Food, LinkMealFood
from meal.serializers import MealSerializer, FoodSearializer, LinkMealFoodSerializer
from rest_framework import generics, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status


class MealViewSet(viewsets.ModelViewSet):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer
    permission_classes = []

    def get_queryset(self):

        user_id = self.request.query_params.get('user_id')
        if user_id is not None:
            user = self.request.user
            if user.is_authenticated:
                return Meal.objects.filter(user=user)
            else:
                return Response({'error': 'Not allowed'}, status=401)

        return Meal.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return MealDetailSerializer
        else:
            return MealSerializer



class LinkMealFoodViewSet(viewsets.ModelViewSet):
    queryset = LinkMealFood.objects.all()
    serializer_class = LinkMealFoodSerializer
    permission_classes = []

    def get_queryset(self):
        meal_id = self.request.query_params.get('meal_id')
        if meal_id is not None:
            return LinkMealFood.objects.filter(meal_id = meal_id)

        return LinkMealFood.objects.all()

    
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def create(self, request, *args, **kwargs):
        meal_id = request.data['meal']
        user = self.request.user
        if user.is_authenticated:
            meal = Meal.objects.filter(user = user, id = meal_id)
            if meal:
                return CreateModelMixin.create(self, request, *args, **kwargs)

        return Response("Not authorized", status.HTTP_401_UNAUTHORIZED)
        



class FoodViewSet(viewsets.ModelViewSet):
    queryset = Food.objects.all()
    serializer_class = FoodSearializer
    permission_classes = []

    def get_queryset(self):
        search = self.request.query_params.get('search')
        if search is not None:
            return Food.objects.filter(name__icontains=search)
        
        return Food.objects.all()
